from rest_framework import views
from rest_framework.response import Response
from django.urls.resolvers import RoutePattern
from django.urls import get_resolver



class APIRootView(views.APIView):
    """
    Basic API root view
    """

    def get(self, request, *args, **kwargs):
        api_urls = []
        for i, base_urls in enumerate(get_resolver().url_patterns):
            if str(base_urls.pattern) == 'api/':
                try:
                    api_urls = get_resolver().url_patterns[i].url_patterns
                except BaseException:
                    return Response({})
                else:
                    break
        
        ret = {}
        for url in api_urls:
            if isinstance(url.pattern, RoutePattern):
                pattern = str(url.pattern)
                namespace = pattern.split('/')[0]
                if namespace:
                    ret[namespace] = request.build_absolute_uri(pattern)

        return Response(ret)