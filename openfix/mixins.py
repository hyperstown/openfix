from django.contrib.auth.mixins import AccessMixin
from rest_framework.permissions import AllowAny
from django.core.exceptions import PermissionDenied


class PermissionsMixin(AccessMixin):
    """
    Allows to use rest_framework permissions inside standard class based views.
    """

    permission_classes = [AllowAny]

    def dispatch(self, request, *args, **kwargs):
        return self.check_permissions(request) or super().dispatch(request, *args, **kwargs) # type:ignore

    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        if not hasattr(self, 'permission_classes') or self.permission_classes is None:
            return []
        return [permission() for permission in self.permission_classes] # type:ignore

    def permission_denied(self, message=None):
        """
        If request is not permitted, determine what kind of exception to raise.
        """
        raise PermissionDenied(message)

    def check_permissions(self, request):
        """
        Check if the request should be permitted.
        Raises an appropriate exception if the request is not permitted.
        """
        if not hasattr(self, 'login_url') or self.login_url is None:
            self.raise_exception = True
        
        for permission in self.get_permissions():
            if not permission.has_permission(request, self):
                if not request.user.is_authenticated:
                    return self.handle_no_permission()
                self.permission_denied(
                    message=getattr(permission, 'message', None)
                )

class FilterMixin:

    def filter_queryset(self, queryset):
        """
        
        """
        for backend in list(getattr(self, 'filter_backends', [])):
            queryset = backend().filter_queryset(self.request, queryset, self)
        return queryset
