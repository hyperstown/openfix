from django.urls import path

from . import views

urlpatterns = [
    path('', views.ProjectsListView.as_view(), name='projects-list'),
    path('new/', views.ProjectsCreateView.as_view(), name='project-create'),
    path('<slug:slug>/members/', views.ProjectAddMemberView.as_view(), name='project-add-member'),
    path('<slug:slug>/', views.ProjectDetailView.as_view(), name='project-detail'),
    path('<slug:slug>/issues/', views.IssuesListView.as_view(), name='issues'),
    path('<slug:slug>/issues/new/', views.IssueCreateView.as_view(), name='issue-create'),
    path('<slug:slug>/issues/<issue_id>/', views.IssueDetailView.as_view(), name='issue-detail'),
    path('<slug:slug>/issues/<issue_id>/add-comment/', views.CommentCreateView.as_view(), name='comment-create'),
    path('<slug:slug>/issues/<issue_id>/add-label/', views.LabelCreateView.as_view(), name='label-create'),
    path('<slug:slug>/issues/<issue_id>/add-assignee/', views.AssigneeUpdateView.as_view(), name='assignee-update'),
    path('<slug:slug>/issues/<issue_id>/add-type/', views.TypeUpdateView.as_view(), name='type-update'),
    path('<slug:slug>/issues/<issue_id>/add-status/', views.StatusUpdateView.as_view(), name='status-update'),
    path('<slug:slug>/issues/<issue_id>/add-severity/', views.SeverityUpdateView.as_view(), name='severity-update'),
    path('<slug:slug>/issues/<issue_id>/add-deadline/', views.DeadlineUpdateView.as_view(), name='deadline-update'),
    path('<slug:slug>/issues/<issue_id>/add-resolution/', views.ResolutionUpdateView.as_view(), name='resolution-update'),
    path('<slug:slug>/issues/<issue_id>/add-references/', views.ReferencesUpdateView.as_view(), name='references-update'),
    path('<slug:slug>/issues/<issue_id>/add-priority/', views.PriorityUpdateView.as_view(), name='priority-update'),
]
