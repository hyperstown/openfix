import re

def int_choices(max_value, min_value=0):
    choices = []
    for value in range(min_value, max_value):
        choices.append((value, str(value)))
    
    return choices

def char_choices(*choices):
    if len(choices) == 1:
        if not isinstance(choices[0], list):
            raise ValueError("Choices must be a list")
        choices = choices[0]
    
    choices_tuple = tuple()
    for choice in choices:
        choices_tuple += ((choice, choice),)

    return choices_tuple

# color utils

class RGB:
    def __init__(self, r, g, b):
        self.r = r
        self.g = g
        self.b = b

def hex_to_rgb(hx, hsl=False):
    if re.compile(r'#[a-fA-F0-9]{3}(?:[a-fA-F0-9]{3})?$').match(hx):
        div = 255.0 if hsl else 0
        if len(hx) <= 4:
            return RGB(*tuple(int(hx[i]*2, 16) / div if div else
                         int(hx[i]*2, 16) for i in (1, 2, 3)))
        return RGB(*tuple(int(hx[i:i+2], 16) / div if div else
                     int(hx[i:i+2], 16) for i in (1, 3, 5)))
    return RGB(255, 255, 255) # returns white if fail to convert

def get_brightness(rgb):
    return (rgb.r * 299 + rgb.g * 587 + rgb.b * 114) / 1000


def is_dark(color_hex):
    return get_brightness(hex_to_rgb(color_hex)) < 128
