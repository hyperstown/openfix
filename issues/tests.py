import json
from urllib import request
from django.test import TestCase
from django.contrib.auth import get_user_model
from django.http import Http404
from django.shortcuts import get_object_or_404

from .models import Issue, Project, Comment

def get_object_or_none(*args, **kwargs):
    try:
        return get_object_or_404(*args, **kwargs)
    except Http404:
        return None

def bool_state(state):
    if state == "open":
        return True
    return False


def init_dummy_issues(url, comments=True, proj=1):
    issues_dict = {}

    for i in range(1, 1000):
        print("page=", i)
        req = request.Request(url + "?per_page=100&state=all" + f"&page={i}", headers={ 'Authorization' : '' })
        response = request.urlopen(req)
        # with open('data.json', 'w') as f:
        #     json.dump(json.loads(response.read()), f)
        #     exit()
        issues =  json.loads(response.read())
        if not issues:
            print("last page break!")
            break


        for issue in issues:
            issues_dict[str(issue.get('number', '0'))] = Issue.objects.get_or_create(
                title=issue.get('title'), description=issue.get('body'), is_open=bool_state(issue.get('state')),
                project=Project.objects.get(id=proj), 
                created_by=get_user_model().objects.get_or_create(
                    username=issue.get('user', {"login", "test3"}).get('login'), password="standardTestPassword4321"
                )[0]
            )

    print("Issues created!")

    if comments:
        req = request.Request(url + '/comments', headers={ 'Authorization' : '' })
        response = request.urlopen(req)
        comments =  json.loads(response.read())

        print("fetching comments")
        for comment in comments:
            issue_id = comment.get('issue_url', 'none').split('/')[-1]
            issue = issues_dict.get(issue_id)
            if issue:
                Comment.objects.get_or_create(
                        issue=issue[0], owner=get_user_model().objects.get_or_create(
                        username=comment.get('user', {"login", "test3"}).get('login'), password="standardTestPassword4321"
                    )[0],
                    content=comment.get('body')
                )
        print("comments created!")


init_dummy_issues('https://api.github.com/repos/IrisShaders/Iris/issues', proj=1)
init_dummy_issues('https://api.github.com/repos/bgrins/TinyColor/issues', proj=2)
init_dummy_issues('https://api.github.com/repos/ventoy/Ventoy/issues', proj=3)
init_dummy_issues('https://api.github.com/repos/lutris/lutris/issues', proj=4)
init_dummy_issues('https://api.github.com/repos/Luwx/Lightly/issues', proj=5)

