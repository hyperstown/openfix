from django_filters import rest_framework as filters
from .models import Issue, Project


class IssueFilterSet(filters.FilterSet):

    class Meta:
        model = Issue
        fields = {
            'title': ['icontains']
        }

class ProjectFilterSet(filters.FilterSet):

    class Meta:
        model = Project
        fields = {
            'name': ['icontains']
        }
