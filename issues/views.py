from django.db.models import Q
from django.urls import reverse
from django.http import QueryDict, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.views.generic.base import TemplateView
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView

from rest_framework.permissions import IsAuthenticated, IsAdminUser, AllowAny

from openfix.mixins import PermissionsMixin, FilterMixin
from .models import Issue, Project, Comment, Label
from openfix.filters import DjangoFilterBackendStandard, StandardSearchFilter
from .filters import IssueFilterSet, ProjectFilterSet
from .permissions import *

from .utils import int_choices



class ProjectAddMemberView(PermissionsMixin, TemplateView):
    
    queryset = Project.objects.order_by('id')
    template_name = "issues/project_members.html"
    permission_classes = [IsProjectMaintainer|IsAdminUser]

    def get(self, request, *args, **kwargs):
        project_name = Project.objects.get(slug=kwargs.get('slug')).name
        self.extra_context = {"project_name": project_name }
        return super().get(request, *args, **kwargs)

class ProjectsCreateView(PermissionsMixin, CreateView):

    model = Project
    permission_classes = [IsAuthenticated]
    fields = ['name', 'description', 'short_description', 'visible', 'members']
    template_name = 'issues/project_new.html'

    def get_success_url(self):
        return reverse('project-detail', kwargs={'slug': self.object.slug})

    def post(self, request, *args, **kwargs):
        response = super().post(request, *args, **kwargs)
        if request.user not in self.object.maintainers.all():
            self.object.maintainers.add(request.user)
        return response

class ProjectsListView(PermissionsMixin, FilterMixin, ListView):

    queryset = Project.objects.order_by('id')
    paginate_by = 15
    login_url = "/login/"
    permission_classes = [AllowAny]
    template_name = 'issues/index.html'
    context_object_name = 'projects'
    filterset_class = ProjectFilterSet
    filter_backends = [StandardSearchFilter, DjangoFilterBackendStandard]
    search_fields = ['name', 'id']

    def get(self, request, *args, **kwargs):
        if not is_user_admin(request):
            if request.user.is_authenticated:
                self.queryset = Project.objects.filter(Q(members=request.user) | Q(visible=True)).distinct().order_by('id')
            else:
                self.queryset = Project.objects.filter(visible=True).distinct().order_by('id')
        self.queryset = self.filter_queryset(self.queryset)
        return super().get(request, *args, **kwargs)


class ProjectDetailView(PermissionsMixin, DetailView):

    model = Project
    login_url = "/login/"
    permission_classes = [IsProjectMember|IsAdminUser]
    template_name = 'issues/project_detail.html'
    context_object_name = 'project'
    slug_url_kwarg = 'slug'
    slug_field = 'slug'

    def get(self, request, *args, **kwargs):
        request.user.is_maintainer = self.model.objects.filter(maintainers=self.request.user).exists()
        if not is_user_admin(request):
            self.queryset = Project.objects.filter(Q(members=request.user) | Q(visible=True)).distinct()
        return super().get(request, *args, **kwargs)


class IssuesListView(PermissionsMixin, FilterMixin, ListView):

    #model = Chat
    queryset = Issue.objects.order_by('-created', '-id')
    paginate_by = 20
    login_url = "/login/"
    permission_classes = [CanViewIssues|IsAdminUser]
    template_name = 'issues/issues_list.html'
    context_object_name = 'issues'
    filterset_class = IssueFilterSet
    filter_backends = [StandardSearchFilter, DjangoFilterBackendStandard]
    search_fields = ['title', 'id']
    extra_context= {}

    def get_queryset(self):
        self.queryset = self.queryset.filter(project__slug=self.kwargs.get('slug'))
        if not is_user_admin(self.request):
            self.queryset = self.queryset.filter(Q(project__members=self.request.user) | Q(project__visible=True)).distinct()
        return super().get_queryset()
    

    def get(self, request, *args, **kwargs):
        q_params = QueryDict(request.GET.urlencode(), mutable=True)
        q_params.pop('page', None)
        
        try:
            project_name = Project.objects.get(slug=kwargs.get('slug')).name
        except:
            project_name = 'Unknown project'

        open_count = self.get_queryset().filter(is_open=True).count()
        closed_count = self.get_queryset().filter(is_open=False).count()

        state = q_params.get('state', 'opened')
        if state == 'opened':
            self.queryset = self.queryset.filter(is_open=True)
        elif state == 'closed':
            self.queryset = self.queryset.filter(is_open=False)

        self.queryset = self.filter_queryset(self.queryset)

        self.extra_context = {
            "state": state, 
            "q_params":q_params.urlencode(),
            "project_name": project_name,
            "open_count": open_count,
            "closed_count": closed_count,
        }
        
        return super().get(request, *args, **kwargs)

class IssueDetailView(PermissionsMixin, DetailView):

    model = Issue
    login_url = "/login/"
    permission_classes = [IsIssueOwnerOrReadOnly|IsAuthenticated]
    template_name = 'issues/issues_detail.html'
    context_object_name = 'issue'

    slug_url_kwarg = 'issue_id'
    slug_field = 'issue_id'

    extra_context= {
        'TYPE_CHOICES': Issue.TYPE_CHOICES,
        'STATUS_CHOICES': Issue.STATUS_CHOICES,
        'RESOLUTION_CHOICES': Issue.RESOLUTION_CHOICES,
        'SEVERITY_CHOICES': Issue.SEVERITY_CHOICES,
        'PRIORITY_CHOICES': int_choices(11, 1)
    }

    def get_queryset(self):
        self.queryset = self.model._default_manager.filter(project__slug=self.kwargs.get('slug'))
        if not is_user_admin(self.request):
            self.queryset = self.queryset.filter(Q(project__members=self.request.user) | Q(project__visible=True)).distinct()
        return self.queryset

    def get(self, request, *args, **kwargs):
        self.queryset = self.model.objects.filter(project__slug=kwargs.get('slug', ''))
        if not is_user_admin(request):
            self.queryset = self.queryset.filter(Q(project__members=request.user) | Q(project__visible=True)).distinct()
        request.user.is_member = self.queryset.filter(project__members=self.request.user).exists()
        return super().get(request, *args, **kwargs)


class IssueCreateView(CreateView):

    model = Issue
    fields = ['title', 'description']
    template_name = 'issues/issue_new.html'
    permission_classes = [IsAuthenticated]


    def get(self, request, *args, **kwargs):
        project_name = Project.objects.get(slug=kwargs.get('slug')).name
        self.extra_context = {"project_name": project_name}
        return super().get(request, *args, **kwargs)

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        form.instance.project = get_object_or_404(
            Project, slug=self.kwargs.get('slug', '')
        )
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('issue-detail', kwargs={
                'slug': self.object.project.slug,
                'issue_id': self.object.issue_id,
            }
        )
    

class CommentCreateView(CreateView):

    model = Comment
    fields = ['content']
    template_name = 'issues/issues_detail.html'
    permission_classes = [IsAuthenticated]

    def get_success_url(self):
        return reverse('issue-detail', kwargs={
                'slug': self.object.issue.project.slug,
                'issue_id': self.object.issue.issue_id,
            }
        )

    def get(self, request, *args, **kwargs):
        return HttpResponseRedirect(reverse('issue-detail', kwargs=kwargs))

    def post(self, request, *args, **kwargs):
        super().post(request, *args, **kwargs)
        return HttpResponseRedirect(reverse('issue-detail', kwargs=kwargs))

    def form_valid(self, form):
        form.instance.owner = self.request.user
        form.instance.issue = get_object_or_404(
            Issue, 
            issue_id=self.kwargs.get('issue_id', ''), 
            project__slug=self.kwargs.get('slug', '')
        )


        if (form.instance.issue.created_by == self.request.user or 
            self.request.user in form.instance.issue.project.members.all()):
            if self.request.POST.get('close_issue'):
                form.instance.issue.is_open = False
                form.instance.issue.save()
            elif self.request.POST.get('reopen_issue'):
                form.instance.issue.is_open = True
                form.instance.issue.save()
        
        return super().form_valid(form)

    


class LabelCreateView(CreateView):

    model = Label
    fields = ['background_color', 'name']
    template_name = 'issues/issues_detail.html'
    permission_classes = [IsAuthenticated]

    def get_success_url(self):
        return reverse('issue-detail', kwargs=self.kwargs)

    def get(self, request, *args, **kwargs):
        return HttpResponseRedirect(reverse('issue-detail', kwargs=kwargs))

    def post(self, request, *args, **kwargs):
        response = super().post(request, *args, **kwargs)
        self.object.issues.add(self.issue_instance)
        return response

    def form_valid(self, form):
        self.issue_instance = get_object_or_404(
            Issue, 
            issue_id=self.kwargs.get('issue_id', ''), 
            project__slug=self.kwargs.get('slug', '')
        )
        form.instance.project = self.issue_instance.project
        return super().form_valid(form)

class GenericIssueDetailUpdateView(UpdateView):

    model = Issue
    template_name = 'issues/issues_error.html'
    permission_classes = [IsAuthenticated]

    slug_url_kwarg = 'issue_id'
    slug_field = 'issue_id'

    def get_success_url(self):
        return reverse('issue-detail', kwargs={
                'slug': self.object.project.slug,
                'issue_id': self.object.issue_id,
            }
        )

    def get(self, request, *args, **kwargs):
        return HttpResponseRedirect(reverse('issue-detail', kwargs=kwargs))

    def post(self, request, *args, **kwargs):
        self.queryset = self.model.objects.filter(
            project__slug=self.kwargs.pop('slug', '')
        )
        self.object = self.get_object()
        form = self.get_form()
        if form.is_valid() and issue_perms(request.user, self.object):
            return self.form_valid(form)
        else:
            return self.form_invalid(form)


class AssigneeUpdateView(GenericIssueDetailUpdateView):

    fields = ['assignee']


class TypeUpdateView(GenericIssueDetailUpdateView):
    
    fields = ['issue_type']


class StatusUpdateView(GenericIssueDetailUpdateView):
    
    fields = ['status']


class SeverityUpdateView(GenericIssueDetailUpdateView):
    
    fields = ['severity']

class DeadlineUpdateView(GenericIssueDetailUpdateView):
    
    fields = ['deadline']

class ReferencesUpdateView(GenericIssueDetailUpdateView):
    
    fields = ['references']

class ResolutionUpdateView(GenericIssueDetailUpdateView):
    
    fields = ['resolution']

class PriorityUpdateView(GenericIssueDetailUpdateView):
    
    fields = ['priority']
