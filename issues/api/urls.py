from django.urls import path
from django.conf.urls import include
from rest_framework_nested import routers
from . import views


router = routers.DefaultRouter()
router.register(r"attachments", views.AttachmentViewSet, basename="attachment")
router.register(r"", views.ProjectAPIViewset, basename="projects")

## generates:
# /projects/
# /projects/{pk}/
# (projects/ is already defined in global urls)

projects_router = routers.NestedDefaultRouter(router, r"", lookup="projects")
projects_router.register(r"issues", views.IssueAPIViewset, basename="issues")
projects_router.register(r"labels", views.LabelAPIViewset, basename="labels")

## generates:
# /projects/
# /projects/{project_slug}/issues/
# /projects/{project_slug}/issues/{pk}/

issues_router = routers.NestedDefaultRouter(projects_router, r"issues", lookup="issues")
issues_router.register(r"comments", views.CommentAPIViewset, basename="comments")
issues_router.register(r"labels", views.IssueLabelAPIViewset, basename="issue-labels")
## generates:
# /projects/
# /projects/{project_slug}/issues/
# /projects/{project_slug}/issues/{pk}/
# /projects/{project_slug}/issues/{issues_issue_id}/comments/
# /projects/{project_slug}/issues/{issues_issue_id}//comments/{pk}/
# /projects/{project_slug}/issues/{issues_issue_id}/labels/
# /projects/{project_slug}/issues/{issues_issue_id}//labels/{pk}/

urlpatterns = [
    path(r"", include(router.urls)),
    path(r"", include(projects_router.urls)),
    path(r"", include(issues_router.urls)),
]