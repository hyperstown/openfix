from rest_framework import serializers

from ..models import Project, Issue, Comment, Label, Attachment
from .fields import SimpleURLFileField
from users.api.serializers import UserSerializer

class AttachmentSerializer(serializers.ModelSerializer):

    attachment = SimpleURLFileField()

    class Meta:
        model = Attachment
        fields = '__all__'


class ProjectSerializer(serializers.ModelSerializer):

    def to_representation(self, instance):
        """ Show owner's username instead id """
        representation = super().to_representation(instance)
        representation['members'] = UserSerializer(instance.members.all(), many=True).data
        return representation 

    class Meta:
        model = Project
        fields = '__all__'


class IssueSerializer(serializers.ModelSerializer):

    class Meta:
        model = Issue
        fields = '__all__'


class CommentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Comment
        fields = '__all__'


class LabelSerializer(serializers.ModelSerializer):

    class Meta:
        model = Label
        fields = '__all__'

