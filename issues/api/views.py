from django.db.models import Q
from django.shortcuts import get_object_or_404
from rest_framework import status, viewsets
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated, IsAdminUser

from ..models import Project, Issue, Comment, Label, Attachment
from ..permissions import IsCommentOwnerOrReadOnly, IsIssueOwnerOrReadOnly, IsProjectMember, is_user_admin
from .serializers import (
    ProjectSerializer, IssueSerializer, 
    CommentSerializer, LabelSerializer, AttachmentSerializer
)

class AttachmentViewSet(viewsets.ModelViewSet):

    permission_classes = [IsAdminUser]
    queryset = Attachment.objects.all()
    serializer_class = AttachmentSerializer

    @action(detail=False, methods=['post'], permission_classes=[IsAuthenticated])
    def add(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class ProjectAPIViewset(viewsets.ModelViewSet):

    permission_classes = [IsProjectMember|IsAdminUser]
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    search_fields = ['name']
    lookup_field = 'slug'
    lookup_url_kwarg = 'slug'

    @action(detail=True, methods=['post'], url_path='append-label')
    def append_label(self, request, *args, **kwargs):
        proj = get_object_or_404(Project, slug=kwargs["slug"])
        issue = proj.issues.get(issue_id=request.data.get('issue_id'))
        label = proj.labels.get(id=request.data.get('label_id'))
        label.issues.add(issue)
        return Response(
            LabelSerializer(issue.labels, many=True).data
        )

    def dispatch(self, request, *args, **kwargs):
        if not is_user_admin(request):
            self.queryset = Project.objects.filter(Q(members=request.user) | Q(visible=True))
        return super().dispatch(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        instance = serializer.save()
        if request.user not in instance.maintainers.all():
            instance.maintainers.add(request.user)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class IssueAPIViewset(viewsets.ModelViewSet):

    permission_classes = [IsIssueOwnerOrReadOnly|IsAdminUser]
    queryset = Issue.objects.all()
    serializer_class = IssueSerializer
    search_fields = ['title']
    lookup_field = 'issue_id'
    lookup_url_kwarg = 'issue_id'

    def get_queryset(self):
        return Issue.objects.filter(project__slug=self.kwargs['projects_slug'])

class CommentAPIViewset(viewsets.ModelViewSet):

    permission_classes = [IsCommentOwnerOrReadOnly|IsAdminUser]
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    search_fields = ['content']

    def get_queryset(self):
        return Comment.objects.filter(
            issue__project__slug=self.kwargs['projects_slug'],
            issue__issue_id=self.kwargs['issues_issue_id']
        )


class LabelAPIViewset(viewsets.ModelViewSet):

    permission_classes = [IsAuthenticated]
    queryset = Label.objects.all()
    serializer_class = LabelSerializer
    search_fields = ['name']

    def get_queryset(self):
        return Label.objects.filter(
            project__slug=self.kwargs['projects_slug']
        )

class IssueLabelAPIViewset(LabelAPIViewset):

    def get_queryset(self):
        return Label.objects.filter(
            project__slug=self.kwargs['projects_slug'],
            issues__issue_id=self.kwargs['issues_issue_id']
        )
