from django.contrib import admin
from .models import Project, Label, Issue, Comment

admin.site.register(Project)
admin.site.register(Label)
admin.site.register(Issue)
admin.site.register(Comment)
