from django.http import Http404
from rest_framework.permissions import BasePermission, SAFE_METHODS


def issue_perms(user, obj):
    return bool(user and user.is_staff or user == obj.created_by or user in obj.project.members.all())

def is_user_admin(request):
    return bool(request.user and request.user.is_staff)

class IsCommentOwnerOrReadOnly(BasePermission):
    
    def _check_owner(self, user, view):
        if view.kwargs.get('pk'):
            try:
                comment = view.get_queryset().get(id=view.kwargs.get('pk'))
            except view.queryset.model.DoesNotExist:
                return False
            return bool(comment.owner == user)
        return False

    def has_permission(self, request, view):
        comment = None
        owner = False
        if view.kwargs.get('pk'):
            try:
                comment = view.get_queryset().get(id=view.kwargs.get('pk'))
            except view.queryset.model.DoesNotExist:
                return False
            return bool(comment.owner == request.user)

        read_only = request.method in (*SAFE_METHODS, 'POST') if comment and comment.issue.project.visible else False
        return bool(
            read_only and request.user or owner
        )


class IsIssueOwnerOrReadOnly(BasePermission):

    def has_permission(self, request, view):
        issue = None
        owner = False
        if view.kwargs.get('issue_id'):
            try:
                issue = view.get_queryset().get(issue_id=view.kwargs.get('issue_id'))
            except view.get_queryset().model.DoesNotExist:
                return False
            owner = bool(issue.created_by == request.user or request.user in issue.project.members.all())
        read_only = request.method in SAFE_METHODS if issue and issue.project.visible else False
        return bool(
            read_only and request.user or owner
        )


class CanViewIssues(BasePermission):

    def has_permission(self, request, view):
        owner = False
        project = None
        if view.kwargs.get('slug'):
            try:
                # it would be easier to just import Project model but it's more interesting that way ^^
                project = view.get_queryset().model._meta.get_field('project').related_model.objects.get(
                    slug=view.kwargs.get('slug')
                )
            except view.get_queryset().model._meta.get_field('project').related_model.DoesNotExist:
                return False
            owner = bool(request.user in project.members.all())
        read_only = request.method in SAFE_METHODS if project and project.visible else False
        return bool(
            read_only and request.user or owner
        )

class IsProjectMember(BasePermission):

    def has_permission(self, request, view):
        owner = False
        # if not request.user.is_authenticated and hasattr(view.__class__, 'get_object'):
        #     if not view.get_object().visible:
        #         return False
        if view.kwargs.get('slug'):
            try:
                project = view.get_queryset().get(slug=view.kwargs.get('slug'))
            except view.queryset.model.DoesNotExist:
                return False
            owner = bool(request.user in project.members.all())

        return bool(
            request.method in (*SAFE_METHODS, 'POST') and request.user or owner
        )


class IsProjectMaintainer(BasePermission):

    def has_permission(self, request, view):
        if view.kwargs.get('slug'):
            try:
                project = view.queryset.get(slug=view.kwargs.get('slug'))
            except view.queryset.model.DoesNotExist:
                return False
            return bool(request.user in project.maintainers.all())

        return False