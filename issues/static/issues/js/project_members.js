let globalData = {
    members: [],
    maintainers: [],
    user: null,
}

function escapeHTML(str){
    return new Option(str).innerHTML;
}

async function updateMembers(members){
    const projectSlug = window.location.pathname.replace('/projects/', '').replace(/\/members[\/]*/, '');
    const csrftoken = getCookie('csrftoken');
    const request = new Request(
        '/api/projects/' + projectSlug + '/',
        {
            method: 'PATCH',
            headers: {
                'X-CSRFToken': csrftoken,
                "Content-type": "application/json; charset=UTF-8"
            },
            mode: 'same-origin', // Do not send CSRF token to another domain.
            body: JSON.stringify({
                members: members,
            }),
        }
    );
    fetch(request).then(function(response) {
        if(!response.ok){
            showError("Error!", response.status);
            console.error(response);
        }
        else{
            fetchMembers(true);
        }
    });
}

async function deleteMember(e){
    let id = e.target.closest('li').id.replace('member-', '');
    let members = globalData.members.map(function (val) {
        return val.id;
    });
    const index = members.indexOf(parseInt(id));
    if (index > -1) {
        members.splice(index, 1); // 2nd parameter means remove one item only
    }
    updateMembers(members);
}

async function fetchMembers(replace=false){
    let listItemTemplate = ``;
    let maintainerTemplate = ``;
    let membersListElement = document.getElementById('project-members-list');

    const projectSlug = window.location.pathname.replace('/projects/', '').replace(/\/members[\/]*/, '');
    const response = await fetch(
        '/api/projects/' + projectSlug + '/'
    ).catch(error => {
        showError('Error fetching project members!');
        console.error(error);
    });


    if(!response){
        showError('Error fetching project members!');
    }
    else if(response && !response.ok){
        showError('Error fetching project members!', response.status);
    }
    else{
        let data = await response.json();
        if(replace){
            membersListElement.innerHTML = '';
        }
    
        globalData.maintainers = data.maintainers;
        globalData.members = data.members;
    
        if(!globalData.members){
            membersListElement.innerHTML = 'Project has no members';
        }
        
        for(member of globalData.members){
            maintainerTemplate = `<span class="badge rounded-pill bg-secondary">Maintainer</span>`;
            deleteButtonTemplate = `
                <div class="page-title-controls">
                    <button type="button" class="btn btn-danger justify-right">Delete Member</button>
                </div>
            `;
    
            if(!globalData.maintainers.includes(member.id)){
                maintainerTemplate = ``;
            };
    
            if(!globalData.maintainers.includes(globalData.user.id) && !globalData.user.is_superuser){
                deleteButtonTemplate = ``;
            };
    
            listItemTemplate = `
                <div class="member d-flex align-items-center">
                    <a href="/" class="no-decoration">
                        ${escapeHTML(member?.first_name || '')} 
                        ${escapeHTML(member?.last_name || '')} 
                        @ ${escapeHTML(member.username)} ${maintainerTemplate}
                    </a>
                    ${deleteButtonTemplate}
                </div>
            `;
            let li = document.createElement('li');
            li.innerHTML = listItemTemplate;
            li.querySelector('button').addEventListener('click', deleteMember)
            li.classList.add('list-group-item');
            li.id = 'member-' + member.id;
            membersListElement.appendChild(li);
        };
    }
};

async function addMember(e){
    let members = globalData.members.map(function (val) {
        return val.id;
    });
    members.push(parseInt(e.target.id));
    updateMembers(members);
}

async function fetchSuggestions(e){
    if(e.target.value){
        const response = await fetch(
            '/api/users?' + new URLSearchParams({
                search: e.target.value,
            })
        ).then(
            response => response.json()
        ).catch(error => {
            showError('Error fetching users!');
            console.error(error);
        });
    
        let members = globalData.members.map(function (val) {
            return val.id;
        });
    
        if(response.results.length && e.target.value.length){
            clearSuggestions();
            for(el of response.results){
                if(!members.includes(el.id)){
                    addSuggestion(`${el.first_name} ${el.last_name} @${el.username}`, el.id);
                }
                
            }
        }
        else{
            document.querySelector('.autocomplete-box').style.display = 'none';
        }
    }
    
};

function addSuggestion(userDetails, userId){
    let newListElement = document.createElement('li');
    newListElement.classList.add('suggestion-element');
    newListElement.appendChild(document.createTextNode(userDetails));
    newListElement.id = userId;

    newListElement.addEventListener("click", addMember);

    let autocompleteBox = document.querySelector('.autocomplete-box ul');
    autocompleteBox.appendChild(newListElement);
    autocompleteBox.parentElement.style.display = 'block';
}

function hideSuggestions(e){
    setTimeout(() => {
        let autocompleteBox = document.querySelector('.autocomplete-box ul');
        autocompleteBox.innerHTML = '';
        autocompleteBox.parentElement.style.display = 'none';
    }, 150);
}

function clearSuggestions(){
    let autocompleteBox = document.querySelector('.autocomplete-box ul');
    autocompleteBox.innerHTML = '';
}

function showError(errorMessage='Server error', code=null){
    if(code){
        errorMessage += ` Code: ${code}`
    }
    let errorElement = document.getElementById('error-message-content');
    errorElement.textContent = errorMessage;
    document.querySelector('.error-message').style.display = 'flex';
}

(async function () {
    globalData.user = await fetch('/api/users/my-account/').then(
        response => response.json()
    ).catch(error => {
        showError('Error fetching user information!');
        console.error(error);
    });
    if(!globalData.user){
        globalData.user = {
            "id": 0,
            "last_login": "",
            "is_superuser": false,
            "username": "anonymous",
            "first_name": "",
            "last_name": "",
            "email": "",
            "is_staff": false,
            "is_active": false,
            "date_joined": "",
            "bio": null,
            "groups": [],
            "user_permissions": []
        }
    }
    fetchMembers(true);
})();
// on runtime

document.getElementById('members-input-wrapper').addEventListener("focusout", hideSuggestions)
document.getElementById('user_select').addEventListener("input", fetchSuggestions);
document.getElementById('user_select').addEventListener("click", fetchSuggestions);