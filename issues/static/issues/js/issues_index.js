const md = window.markdownit({ 
    html: true, 
    linkify: true, 
    highlight: function (code, lang) { 
        const language = hljs.getLanguage(lang) ? lang : 'plaintext'; 
        return hljs.highlight(code, { language }).value; 
    }, 
});

// format issue description
for(let el of document.getElementsByClassName('description')){
    el.innerHTML = DOMPurify.sanitize(md.render(el.textContent.trim()));
}

// format issue comments
for(let el of document.getElementsByClassName('comment-content')){
    el.innerHTML = DOMPurify.sanitize(md.render(el.textContent.trim()));
}

// format comment preview
document.getElementById('preview-tab')?.addEventListener('click', e => { 
    document.getElementById('write-comment-content').innerHTML = DOMPurify.sanitize(
        md.render(document.querySelector('textarea.write-textarea').value)
    ); 
});


//comment-delete-button
//api/projects/test-project-1/issues/31/comments/6/

for(let el of document.getElementsByClassName('comment-delete-button')){
    el.addEventListener('click', function(e){
        const commentId = e.target.closest('li').id.replace('comment-', '');
        let issuePath = window.location.pathname;
        if (issuePath[issuePath.length - 1] !== '/'){
            issuePath += '/'
        }
        const csrftoken = getCookie('csrftoken');
        const request = new Request(
            '/api' + issuePath + 'comments/' + commentId + '/',
            {
                method: 'DELETE',
                headers: {
                    'X-CSRFToken': csrftoken,
                    "Content-type": "application/json; charset=UTF-8"
                },
                mode: 'same-origin', // Do not send CSRF token to another domain.
            }
        );
        fetch(request).then(function(response) {
            if(!response.ok){
                showError("Error!", response.status);
                console.error(response);
            }
            else{
                e.target.closest('li').remove();
            }
        });

    })
}

const insertAt = (str, sub, pos) => `${str.slice(0, pos)}${sub}${str.slice(pos)}`;

async function sendAttachment(e) {
    let clipboardItem = Array.from(e.clipboardData.items).find(x => /^image\//.test(x.type));
    let caretAt = e.target.selectionStart;
   
    if(clipboardItem){
        e.preventDefault();
        let blob = clipboardItem.getAsFile();
        if(blob.type === "image/png"){
            let formData = new FormData();
            formData.append('attachment', blob);
            const csrftoken = getCookie('csrftoken');
            const request = new Request(
                '/api/projects/attachments/add/',
                {
                    method: 'POST',
                    headers: { 'X-CSRFToken': csrftoken },
                    mode: 'same-origin', // Do not send CSRF token to another domain.
                    body: formData
                }
            );
            let response = await fetch(request).then(function(response) {
                if(response.status !== 201){
                    console.error(response);
                }
                else{
                    return response.json();
                }
            });
            
            if(response){
                let attachmentURL ="![image](" + response.attachment + ")";
                e.target.value = insertAt(e.target.value, attachmentURL , caretAt);
                e.target.selectionEnd = caretAt + attachmentURL.length;
            }
        }
    }
}

document.getElementById('new-issue-text-area')?.addEventListener("paste", sendAttachment);
