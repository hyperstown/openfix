let globalData = {
    users: [],
    maintainers: [],
    user: null,
}

function escapeHTML(str){
    return new Option(str).innerHTML;
}

async function refreshUsersList(){
    fetchUsers(true);
}

async function deleteMember(e){
    let id = e.target.closest('li').id.replace('user-', '');
    globalData.users = globalData.users.filter(v => v.id !== parseInt(id));

    let option = document.getElementById('user-input-' + id);
    let ref = document.getElementById('id_members');
    ref.removeChild(option);
    refreshUsersList();
}

async function fetchUsers(replace=false){
    let listItemTemplate = ``;
    let maintainerTemplate = ``;
    let usersListElement = document.getElementById('project-users-list');
    let ref = document.getElementById('id_members');

    if(replace){
        usersListElement.innerHTML = '';
        ref.innerHTML = '';
    }

    if(!globalData.users){
        usersListElement.innerHTML = 'Project has no users';
    }
    
    for(user of globalData.users){
        maintainerTemplate = `
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="is-maintainer-${user.id}" checked>
                <label class="form-check-label" for="is-maintainer">
                    maintainer
                </label>
            </div>
        `;
        deleteButtonTemplate = `
            <div class="page-title-controls">
                <button type="button" class="btn btn-danger justify-right">Delete</button>
            </div>
        `;

        if(!globalData.maintainers.includes(user.id)){
            maintainerTemplate = ``;
        };

        listItemTemplate = `
            <div class="user d-flex align-items-center">
                <a href="/" class="no-decoration">
                    ${escapeHTML(user?.first_name || '')} 
                    ${escapeHTML(user?.last_name || '')} 
                    @ ${escapeHTML(user.username)} ${maintainerTemplate}
                </a>
                ${deleteButtonTemplate}
            </div>
        `;

        let li = document.createElement('li');
        li.innerHTML = listItemTemplate;
        li.querySelector('button').addEventListener('click', deleteMember)
        li.classList.add('list-group-item');
        li.id = 'user-' + user.id;
        usersListElement.appendChild(li);

        let option = document.createElement('option');
        option.id = 'user-input-' + user.id;
        option.selected = true;
        option.value = user.id;
        
        ref.appendChild(option);
    };
    
};

async function addMember(e){
    let users = globalData.users.map(function (val) {
        return val.id;
    });
    users.push(parseInt(e.target.id));
    refreshUsersList();
}

async function fetchSuggestions(e){
    if(e.target.value){
        const response = await fetch(
            '/api/users?' + new URLSearchParams({
                search: e.target.value,
            })
        ).then(
            response => response.json()
        ).catch(error => {
            showError('Error fetching users!');
            console.error(error);
        });
    
        let users = globalData.users.map(function (val) {
            return val.id;
        });
    
        if(response.results.length && e.target.value.length){
            clearSuggestions();
            // make it let i etc
            for(el of response.results){
                if(!users.includes(el.id)){
                    let newListElement = document.createElement('li');
                    newListElement.classList.add('suggestion-element');
                    newListElement.appendChild(document.createTextNode(`${el.first_name} ${el.last_name} @${el.username}`, el.id));
                    newListElement.id = el.id;
                    newListElement.userData = el;
                    newListElement.addEventListener("click", function(e){
                        globalData.users.push(e.target.userData);
                        refreshUsersList();
                    });
                    let autocompleteBox = document.querySelector('.autocomplete-box ul');
                    autocompleteBox.appendChild(newListElement);
                    autocompleteBox.parentElement.style.display = 'block';
                }
            }
        }
        else{
            document.querySelector('.autocomplete-box').style.display = 'none';
        }
    }
    
};

function addSuggestion(userDetails, userId){
    let newListElement = document.createElement('li');
    newListElement.classList.add('suggestion-element');
    newListElement.appendChild(document.createTextNode(userDetails));
    newListElement.id = userId;

    newListElement.addEventListener("click", addMember);

    let autocompleteBox = document.querySelector('.autocomplete-box ul');
    autocompleteBox.appendChild(newListElement);
    autocompleteBox.parentElement.style.display = 'block';
}

function hideSuggestions(e){
    setTimeout(() => {
        let autocompleteBox = document.querySelector('.autocomplete-box ul');
        autocompleteBox.innerHTML = '';
        autocompleteBox.parentElement.style.display = 'none';
    }, 150);
}

function clearSuggestions(){
    let autocompleteBox = document.querySelector('.autocomplete-box ul');
    autocompleteBox.innerHTML = '';
}

function showError(errorMessage='Server error', code=null){
    if(code){
        errorMessage += ` Code: ${code}`
    }
    let errorElement = document.getElementById('error-message-content');
    errorElement.textContent = errorMessage;
    document.querySelector('.error-message').style.display = 'flex';
}

(async function () {
    globalData.user = await fetch('/api/users/my-account/').then(
        response => response.json()
    ).catch(error => {
        showError('Error fetching user information!');
        console.error(error);
    });
    if(!globalData.user){
        globalData.user = {
            "id": 0,
            "last_login": "",
            "is_superuser": false,
            "username": "anonymous",
            "first_name": "",
            "last_name": "",
            "email": "",
            "is_staff": false,
            "is_active": false,
            "date_joined": "",
            "bio": null,
            "groups": [],
            "user_permissions": []
        }
    }
    fetchUsers(true);
})();
// on runtime

document.getElementById('users-input-wrapper').addEventListener("focusout", hideSuggestions)
document.getElementById('user_select').addEventListener("input", fetchSuggestions);
document.getElementById('user_select').addEventListener("click", fetchSuggestions);