document.getElementById('color-picker').addEventListener('change', function(e){
    document.getElementById('selected-color').textContent = e.target.value;
});

document.getElementById('selected-color').textContent = document.getElementById('color-picker').value;

document.getElementById('select-label').addEventListener('click', async function(e){
    e.preventDefault();
    const projectName = window.location.pathname.replace(/\/projects\//, '').replace(/\/issues\/[0-9]+\//, '')
    const issueNr =  window.location.pathname.split('/').filter(v => v).reverse()[0];
    const labelId = document.getElementById('id_labels').value;

    const csrftoken = getCookie('csrftoken');
    const request = new Request(
        '/api/projects/' + projectName + '/append-label/',
        {
            method: 'POST',
            headers: {
                'X-CSRFToken': csrftoken,
                "Content-type": "application/json; charset=UTF-8"
            },
            mode: 'same-origin', // Do not send CSRF token to another domain.
            body: JSON.stringify(
                {
                    issue_id: issueNr,
                    label_id: labelId
                }
            )
        }
    );
    const response = await fetch(request).then(function(response) {
        if(!response.ok){
            // showError("Error!", response.status);
            console.error(response);
        }
        else{
            return response.json()
        }
    });

    console.log(response)

    let labelsList = document.querySelector('.labels-list')
    for(el of labelsList.querySelectorAll('.badge')){
        el.remove();
    }
    for(label of response){
        let addLabelIcon = labelsList.querySelector('.add-label-icon');
        let badge = document.createElement('span')
        badge.classList.add('badge');
        badge.style = `background-color:${ label.background_color }; color: ${ label.color }; margin-right:5px`
        badge.textContent = label.name;
        labelsList.insertBefore(badge, addLabelIcon);

        let labelOption = document.getElementById('label-option-' + label.id)
        if(labelOption){
            labelOption.remove();
        }
    }
})


document.getElementById('comment-content-textarea').addEventListener("paste", sendAttachment);

$(".collapse").on("hidden.bs.collapse", function() {
    localStorage.setItem("coll_" + this.id, false);
});

$(".collapse").on("shown.bs.collapse", function() {
    localStorage.setItem("coll_" + this.id, true);
});

$(".collapse").each(function() {
    if (localStorage.getItem("coll_" + this.id) == "true") {
        this.classList.add('show')
    }
});