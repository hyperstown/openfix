import re
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

def validate_color(value):
    if not re.compile(r'#[a-fA-F0-9]{3}(?:[a-fA-F0-9]{3})?$').match(value):
        raise ValidationError(
            _('%(value)s is not a valid color'),
            params={'value': value},
        )