from django.shortcuts import render
from django.template import loader
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required

from issues.models import Issue, Project, Comment

# class someView(LoginRequiredMixin, CreateView):
#     login_url = "/login/"

@login_required(login_url='/login/')
def index(request):
    template = loader.get_template('home/index.html')
    context = {
        "issues": Issue.objects.filter(created_by=request.user, is_open=True).order_by('-created')[:5],
        "projects": Project.objects.filter(members=request.user).order_by('-id')[:5],
        "comments": Comment.objects.filter(owner=request.user).order_by('-timestamp')[:5],
    }
    return HttpResponse(template.render(context, request))
